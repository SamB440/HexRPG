# HexRPG

This is a continued version of ItsJasonn's HexRPG.

# Contributing

All contributions to the source must be compatible with the latest version of Spigot (and PaperSpigot), and Java 8.

## Code Rules

Use lambda where possible. For example;

This is acceptable.

```java
Bukkit.getScheduler().runTask(plugin, () -> {
    System.out.println("Yay! Lambda!");
});
```
```java
Bukkit.getScheduler().runTask(plugin, () -> System.out.println("Yay! Lambda with single line!"));
```

This is not acceptable.

```java
Bukkit.getScheduler().runTask(plugin, new Runnable() {
    @Override
    public void run() {
        System.out.println("Boo! Unclean code with no lambda!");
    }
});
```

# Exporting

Be aware that sharing the jar is NOT allowed - you can only use it yourself! This is due to the rules imposed by ItsJasonn.