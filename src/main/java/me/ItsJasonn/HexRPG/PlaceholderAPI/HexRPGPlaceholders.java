package me.ItsJasonn.HexRPG.PlaceholderAPI;

import org.bukkit.entity.Player;

import me.ItsJasonn.HexRPG.Main.Core;
import me.ItsJasonn.HexRPG.Main.Plugin;
import me.ItsJasonn.HexRPG.Tools.PlayerLevel;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class HexRPGPlaceholders extends PlaceholderExpansion {
	
	private Core plugin;
	
	public HexRPGPlaceholders(Core core) {
		this.plugin = core;
	}

	@Override
	public String onPlaceholderRequest(Player player, String cmd) {
		PlayerLevel playerLevel = new PlayerLevel(player);
		
		if(cmd.equalsIgnoreCase("maxhp") || cmd.equalsIgnoreCase("maxHealth")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerMaxHealth().get(player));
		} else if(cmd.equalsIgnoreCase("hp") || cmd.equalsIgnoreCase("health")) {
			return String.valueOf(player.getHealth());
		} else if(cmd.equalsIgnoreCase("damage")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerDPS().get(player));
		} else if(cmd.equalsIgnoreCase("defense")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerDefense().get(player));
		} else if(cmd.equalsIgnoreCase("regen") || cmd.equalsIgnoreCase("regeneration")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getHpRegenerationCount().get(player.getName()));
		} else if(cmd.equalsIgnoreCase("agility")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerAgility().get(player));
		} else if(cmd.equalsIgnoreCase("strength")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerStrength().get(player));
		} else if(cmd.equalsIgnoreCase("endurance")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerEndurance().get(player));
		} else if(cmd.equalsIgnoreCase("dexterity")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerDexterity().get(player));
		} else if(cmd.equalsIgnoreCase("criticalChance")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerCriticalChance().get(player));
		} else if(cmd.equalsIgnoreCase("criticalDamage")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerCriticalDamage().get(player));
		} else if(cmd.equalsIgnoreCase("lifeSteal")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getPlayerLifesteal().get(player));
		} else if(cmd.equalsIgnoreCase("class")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getClass(player));
		} else if(cmd.equalsIgnoreCase("race")) {
			return String.valueOf(Plugin.getCore().getStatsManager().getRace(player));
		} else if(cmd.equalsIgnoreCase("exp") || cmd.equalsIgnoreCase("xp")) {
			return String.valueOf(playerLevel.getExp());
		} else if(cmd.equalsIgnoreCase("level")) {
			return String.valueOf(playerLevel.getLevel());
		} else if(cmd.equalsIgnoreCase("requiredExp") || cmd.equalsIgnoreCase("requiredXp")) {
			return String.valueOf(playerLevel.getRequiredExp());
		} else if(cmd.equalsIgnoreCase("woodcutting_level")) {
			return String.valueOf(playerLevel.getSkillLevel("WOODCUTTING"));
		} else if(cmd.equalsIgnoreCase("farming_level")) {
			return String.valueOf(playerLevel.getSkillLevel("FARMING"));
		} else if(cmd.equalsIgnoreCase("mining_level")) {
			return String.valueOf(playerLevel.getSkillLevel("MINING"));
		} else if(cmd.equalsIgnoreCase("hitpoints_level")) {
			return String.valueOf(playerLevel.getSkillLevel("HITPOINTS"));
		} else if(cmd.equalsIgnoreCase("fishing_level")) {
			return String.valueOf(playerLevel.getSkillLevel("FISHING"));
		} else if(cmd.equalsIgnoreCase("smithing_level")) {
			return String.valueOf(playerLevel.getSkillLevel("SMITHING"));
		} else if(cmd.equalsIgnoreCase("master_level")) {
			return String.valueOf(playerLevel.getSkillLevel("MASTER"));
		}
		
		return null;
	}

    /**
     * Because this is an internal class,
     * you must override this method to let PlaceholderAPI know to not unregister your expansion class when
     * PlaceholderAPI is reloaded
     *
     * @return true to persist through reloads
     */
	@Override
    public boolean persist() {
        return true;
    }

    /**
     * Because this is a internal class, this check is not needed
     * and we can simply return {@code true}
     *
     * @return Always true since it's an internal class.
     */
    @Override
    public boolean canRegister() {
        return true;
    }

    /**
     * The name of the person who created this expansion should go here.
     * <br>For convienience do we return the author from the plugin.yml
     * 
     * @return The name of the author as a String.
     */
    @Override
    public String getAuthor() {
        return plugin.getDescription().getAuthors().toString();
    }

    /**
     * The placeholder identifier should go here.
     * <br>This is what tells PlaceholderAPI to call our onRequest 
     * method to obtain a value if a placeholder starts with our 
     * identifier.
     * <br>This must be unique and can not contain % or _
     *
     * @return The identifier in {@code %<identifier>_<value>%} as String.
     */
    @Override
    public String getIdentifier() {
        return "hexrpg";
    }

    /**
     * This is the version of the expansion.
     * <br>You don't have to use numbers, since it is set as a String.
     *
     * For convienience do we return the version from the plugin.yml
     *
     * @return The version as a String.
     */
    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }
}